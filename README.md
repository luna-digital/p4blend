# P4Blend Add-on for Blender
P4Blend integrates Blender with the Perforce versioning system. We are currently developing P4Blend in-house at [Luna Digital](http://www.lunadigital.tv), using it in production on various client projects. For more information about in-house development, contact us at hello@lunadigital.tv.

# Installation Notes
P4Blend assumes you have a working install of Perforce, with variables already set for P4PORT, P4CLIENT, and P4USER. If you can successfully run Perforce p4 commands in a terminal, Blender should handle things just fine. Future versions will let you set these variables in your Blender add-on preferences.

For more information about installing and configuring P4, visit the following pages:

* [Installing P4](https://www.perforce.com/perforce/r14.2/manuals/p4guide/chapter.install.html)
* [Configuring P4](https://www.perforce.com/perforce/r15.1/manuals/p4guide/chapter.configuration.html)

# Current Limitations
It is currently only possible to check out a file from within Blender. Future updates will allow you to switch changelists and submit your blend file to the Perforce server directly from Blender's UI.
