# P4Blend
# Contributor(s): Aaron Powell (aaron@lunadigital.tv)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy
from bpy.types import Panel, Operator

from . import engine

class RefreshOperator(Operator):
    bl_idname = "p4.refresh"
    bl_label = ""
    
    def execute(self, context):
        if (engine.is_checked_out()):
            context.scene.checked_out = True
        else:
            context.scene.checked_out = False

        return {'FINISHED'}

class CheckoutOperator(Operator):
    bl_idname = "p4.checkout"
    bl_label = "Checkout File"

    @classmethod
    def poll(cls, context):
        return not context.scene.checked_out

    def execute(self, context):
        engine.checkout(bpy.data.filepath)
        context.scene.checked_out = True
        return {'FINISHED'}

class PerforcePanel(Panel):
    bl_label = 'Perforce'
    bl_space_type = 'PROPERTIES'
    bl_region_type= 'WINDOW'
    bl_context = 'render'

    def draw_header(self, context):
        self.layout.operator("p4.refresh", icon="FILE_REFRESH")

    def draw(self, context):
        row = self.layout.row()
        row.operator("p4.checkout", icon="FILE_TICK")

def register():
    bpy.utils.register_class(RefreshOperator)
    bpy.utils.register_class(CheckoutOperator)
    bpy.utils.register_class(PerforcePanel)

def unregister():
    bpy.utils.unregister_class(RefreshOperator)
    bpy.utils.unregister_class(CheckoutOperator)
    bpy.utils.unregister_class(PerforcePanel)
