# P4Blend
# Contributor(s): Aaron Powell (aaron@lunadigital.tv)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy
import subprocess
import os

def switch_workspace(workspace):
    command = "P4CLIENT=" + str(workspace)
    process = subprocess.Popen(["p4", "set", command], shell=True)
    process.communicate()

def get_workspace_root():
    output = subprocess.check_output(["p4", "-F", "%clientRoot%", "-ztag", "info"]).decode("utf-8")
    output = output.split('\r')[0]
    return output
    
def add(filepath, changelist="default"):
    process = subprocess.Popen(["p4", "add", "-d", "-c", changelist, filepath], shell=True)
    process.communicate()

def checkout(filepath, changelist="default"):
    process = subprocess.Popen(["p4", "edit", "-c", changelist, filepath], shell=True)
    process.communicate()

def is_checked_out():
    files = subprocess.check_output(["p4", "opened"]).decode("utf-8")
    
    # A bunch of voodoo magic
    files = files.split('\n')                           # Split output into lines
    del files[-1]                                        # Remove empty item from list
    files = [f.split('#')[0] for f in files]      # Remove unnecessary info
    files = [f.split('//')[1] for f in files]
    files = [get_workspace_root() + "\\" + f for f in files ]
    files = [os.path.normpath(f) for f in files]  # Normalize file path

    if bpy.data.filepath in files:
        return True
    else:
        return False

def register():
    pass

def unregister():
    pass
